import urllib.request
import json

with open("key1.txt") as f:
    key1 = f.read().rstrip("\n")

    base_url = "https://api.clashroyale.com/v1"

    endpoint = "/players/%23"

    player = "PRCVQCG2P"

    request = urllib.request.Request(
        base_url + endpoint + player,
        None,
        dict(Authorization="Bearer %s" % key1)
    )

    response = urllib.request.urlopen(request).read().decode("utf-8")

    data = json.loads(response)
    json.dumps(data, indent=4, separators=(".","="))
