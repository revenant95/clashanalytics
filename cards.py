import urllib.request
import json

with open("key1.txt") as f:
	key1 = f.read().rstrip("\n")
	base_url = "https://api.clashroyale.com/v1"
	endpoint = "/cards"

	request = urllib.request.Request(
			base_url+endpoint,
			None,
			{
				"Authorization": "Bearer %s" % key1
			}
		)

	response = urllib.request.urlopen(request).read().decode("utf-8")
	json_response = json.loads()


	print(response)
