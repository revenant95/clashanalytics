import sys
import urllib.request
import json


file = open("key1.txt")
apiKey = file.read().rstrip("\n")
base_url = "https://api.clashroyale.com/v1"


def query(endpoint):
    request = urllib.request.Request(
        base_url + endpoint, None, dict(Authorization="Bearer %s" % apiKey))
    response = urllib.request.urlopen(request).read().decode("utf-8")
    file.close()
    return json.loads(response)


def cards():
    return query("/cards")


def player(player_id):
    return query("/players/%23" + player_id)


def battle_log(player_id):
    return query("/players/%23" + player_id + "/battlelog")


# argv comes with two arguments: 1) operation 2)argument (optional)
def main(argv):
    operation = argv[1]
    if operation == "cards":
        cards()
    elif operation == "player":
        player(argv[2])
    elif operation == "battlelog":
        battle_log(argv[2])


if __name__ == '__main__':
    main(sys.argv)